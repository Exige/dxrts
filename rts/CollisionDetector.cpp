/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Collision detector

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

#include <algorithm>
#include "CollisionDetector.h"
#include "ActorCollection.h"
#include "Class.h"


CCollisionDetector::CCollisionDetector()
{
}


CCollisionDetector::~CCollisionDetector()
{
}

void CCollisionDetector::addDynamicActor(CActor &act)
{
	m_dynamic.push_back(&act);
}

void CCollisionDetector::addStaticActor(CActor &act)
{
	m_static.push_back(&act);
}


bool compareIntervals(endnode_s a, endnode_s b)
{
	float s1 = a.val;
	float s2 = b.val;

	return s1 < s2;
}


// Collision detection works but it is kinda hacky at the moment.
// Since the field is tilted 45 degrees, I have to first project the coordinates
// onto a 45 degrees rotated x and y axes (Represented by 2 vectors: [1, 1] for the x-axis
// [-1, 1] for the y axis. Maybe I should consider rewriting the drawing code so this can be done
// without all these clunky math operations that take computing power.
void CCollisionDetector::detect(CActorCollection *c)
{
	m_xnodes.clear();
	m_ynodes.clear();
	for (int i = 0; i < c->size(); i++)
	{
		endnode_s node;
		CActor *act = c->getActorFromId(i);

		// get x intervals.
		node.actor = act;		
		node.val = act->getXOrthogonalProjection();
		node.isEnd = false;
		m_xnodes.push_back(node);		
		node.val = act->getXOrthogonalProjection() + cos(glm::pi<float>()/4)*act->getClass()->width();
		node.isEnd = true;
		m_xnodes.push_back(node);
	}

	// Sort the list of intervals.
	std::sort(m_xnodes.begin(), m_xnodes.end(), compareIntervals);
	std::sort(m_xnodes.begin(), m_xnodes.end(), compareIntervals);

	// Create a vector of active intervals.
	std::vector<interval_s> active_x;		

	// Keep track of actual collisions.
	std::vector<collision_s> collisions;

	for (int i = 0; i < m_xnodes.size(); i++)
	{
		if (!m_xnodes[i].isEnd)
		{
			// Start point -> store in active intervals.
			interval_s l;
			CActor *a = m_xnodes[i].actor;
			l.actor = a;
			l.start = m_xnodes[i].val;
			l.end = m_xnodes[i].val + a->getClass()->width();
			active_x.push_back(l);
		}
		else
		{
			// Endpoint -> remove corresponding node from active list.
			for (int j = 0; j < active_x.size(); j++)
			{
				// Search for the corresponding interval in the active list.
				if (active_x[j].actor == m_xnodes[i].actor)
				{
					active_x.erase(active_x.begin() + j, active_x.begin() + j + 1);
					break;
				}
			}
		}

		// Check actual collisions.
		if (active_x.size() > 1)
		{
			// Now also check if they also collide on the y-axis.
			// Any intervals in the active list are colliding.		
			for (int j = 0; j < active_x.size(); j++)
			{
				// Brute force check the y-axes. 
				// TODO: a test later when more objects are involved to look for faster ways, possibly a 2 axes.
				for (int l = 0; l < active_x.size(); l++)
				{
					if (j == l)
						continue;

					CActor *a1 = active_x[j].actor;
					CActor *a2 = active_x[l].actor;

					float y1 = a1->getYOrthogonalProjection();
					float y2 = a2->getYOrthogonalProjection();

					float yw1 = y1 + cos(glm::pi<float>() / 4)*a1->getClass()->width();
					float yw2 = y2 + cos(glm::pi<float>() / 4)*a2->getClass()->width();

	
					if ((y1 > y2 && y1 < yw2) || (yw1 < yw2 && yw1 > y2))
					{
						// Move any colliding object to their previous position.
						active_x[j].actor->setToLastPosition();			

						// Remove current destination.
						active_x[j].actor->resetDestination();
					}
				}
			}
		}
	}
}

