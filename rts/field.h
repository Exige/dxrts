/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Tile code

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

// Include standard headers
#include <stdio.h>
#include <stdlib.h>

// Include GLEW. Always include it before gl.h and glfw.h, since it's a bit magic.
#include "GL/glew.h"

// Include GLFW
#include <GLFW/glfw3.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
//#include <glm/transform.hpp>
//<glm / gtc / matrix_transform.hpp>

#include "shading.h"

enum fieldtype
{
	TYPE_GRASS,
	TYPE_STONE,
};



class CField
{
public:
	void draw(float x, float y, float r, float g, float b);
	CField();

	void setType(int type);

	int type();

	int n;

	bool impassable();

private:
	// Color.
	int r, g, b;

	// Position in the buffer.
	int m_pos;

	// Nothing else can stand on this field.
	// In case of a building.
	bool m_impassable;

	// Field type.
	// Grass, stone, etc.
	int m_type;
};