/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Collections of actors

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

#pragma once

#include <vector>
#include "Graphics.h"
#include "CollisionDetector.h"
#include "Actor.h"



class CActorCollection :
	public CDrawAble
{
public:
	CActorCollection();
	~CActorCollection();

	void draw();
	bool spawn(int x, int y);

	CActor* getActorFromId(int i) { return &m_actors[i]; }
	unsigned int size() { return m_actors.size(); }
	

private:
	std::vector<CActor> m_actors;
	CCollisionDetector m_collisiondetector;
};

