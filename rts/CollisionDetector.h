/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Collision detector

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/
#pragma once

#include <vector>
#include "Actor.h"

class CActorCollection;

struct interval_s
{
	float start;
	float end;
	CActor *actor;
};

struct collision_s
{
	CActor *a;
	CActor *b;
};

struct endnode_s
{
	// Actor owner.
	CActor *actor;

	// Position of this endpoint.
	float val;

	// Start or end point?
	bool isEnd;
};


class CCollisionDetector
{
public:
	CCollisionDetector();
	~CCollisionDetector();

	void addStaticActor(CActor &act);
	void addDynamicActor(CActor &act);

	void detect(CActorCollection *c);
private:
	// Static actors cannot move.
	std::vector<CActor*> m_static;

	// Dynamic actors can move.
	std::vector<CActor*> m_dynamic;

	std::vector<endnode_s> m_xnodes;
	std::vector<endnode_s> m_ynodes;
};

