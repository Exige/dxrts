/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Actor code, object detection etc.

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

#include <glm/gtx/rotate_vector.hpp>
#include "Actor.h"
#include "Graphics.h"
#include "Graphics.h"


CActor::CActor(CTexture *tex)
{
	m_x = 0.0;
	m_y = 0.0;
	m_destx = 0.0;
	m_desty = 0.0;

	m_texture = tex;
	m_changed = false;
	m_selected = false;

	m_lastx = 0.0;
	m_lasty = 0.0;

	m_class = new CUnit("hi", 1);
}


CActor::~CActor()
{
}

void CActor::tick()
{
	g_screen.drawSprite(m_x, m_y, 0.001, *m_texture);
	//g_screen.drawQuad(m_x, m_y, 0.1, CCcolor(0.0, 1.0, 0.0));
	move();
}

float CActor::x()
{
	return m_x;
}

float CActor::y()
{
	return m_y;
}

bool CActor::isSelected()
{
	return m_selected;
}

void CActor::select()
{
	m_selected = true;
}

void CActor::move()
{
	static int keep = 0;
	keep++;
	m_lastx = m_x;
	m_lasty = m_y;	

	if (m_x < m_destx)
	{
		m_x += 0.2;
		m_changed = true;
	}
	if (m_x > m_destx)
	{
		m_x -= 0.2;
		m_changed = true;
	}

	if (m_y < m_desty)
	{
		m_y += 0.2;
		m_changed = true;
	}
	if (m_y > m_desty)
	{
		m_y -= 0.2;
		m_changed = true;
	}	
	
	if (m_changed && keep >= 100)
	{
		keep = 0;
		printf("%f\n", m_x);
		
	}
}


void CActor::setDestination(float x, float y)
{
	m_destx = x;
	m_desty = y;
}

void CActor::deselect()
{
	m_selected = false;
}

void CActor::setPosition(float x, float y)
{
	m_x = x;
	m_y = y;
}

void CActor::setToLastPosition()
{
	m_x = m_lastx;
	m_y = m_lasty;
}

void CActor::resetDestination()
{
	m_destx = m_x;
	m_desty = m_y;
}

// Get the x coordinate on the orthogonal projection on the 45 degree tilted x-axis.
float CActor::getXOrthogonalProjection()
{
	float tx = m_x + 1;
	float ty = m_y - (cos(glm::pi<float>() / 4) * 2 - 1);
	glm::vec2 v(tx, ty);

	glm::vec2 s(1, 1);
	
	glm::vec2 r = (glm::dot(v, s) / glm::dot(s, s))*s;

	return r.x;
}

float CActor::getYOrthogonalProjection()
{
	float tx = m_x + 1;
	float ty = m_y - (cos(glm::pi<float>() / 4) * 2 - 1);
	glm::vec2 v(tx, ty);

	glm::vec2 s(-1, 1);

	glm::vec2 r = (glm::dot(v, s) / glm::dot(s, s))*s;

	return r.y;
}

// Euclidian distance between 2 actors.
float CActor::distance(CActor *other)
{
	float ox = other->x();
	float oy = other->y();

	float dx = abs(ox - m_x);
	float dy = abs(oy - m_y);

	return sqrt(dx*dx + dy*dy);
}