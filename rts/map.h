/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: A map is a collection of tiles (CField)

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/


#include <vector>
#include "field.h"
#include "Graphics.h"

class CMap : public CDrawAble
{
public:
	CMap();
	void draw();

	void create();

	bool occupied(int x, int y);

private:
	CField **m_map;

	int m_offset;

	GLuint m_programID;
	//CDrawAble *m_drawer;
};

extern CMap *g_map;

// Define the cube that will hold our field.
