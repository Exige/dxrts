/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Actor code, object detection etc.

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/


#pragma once

#include "Texture.h"
#include "Class.h"

class CActor
{
public:
	CActor(CTexture *tex);
	~CActor();
	
	void tick();

	float x();
	float y();

	float distance(CActor *other);

	void select();
	void deselect();
	bool isSelected();

	CTexture *texture() { return m_texture; }

	void setDestination(float x, float y);
	void move();

	bool changed() { return m_changed; }
	bool setChanged(bool c) { m_changed = c; }

	void setPosition(float x, float y);
	void setToLastPosition();
	void resetDestination();

	CClass *getClass() { return m_class; }

	float getXOrthogonalProjection();
	float getYOrthogonalProjection();

private:
	CTexture *m_texture;

	// Changed since last tick.
	bool m_changed;

	// Current position.
	float m_x;
	float m_y;	

	// Keep previous x and y coordinates.
	float m_lastx;
	float m_lasty;

	// Desired position
	float m_destx;
	float m_desty;

	bool m_selected;

	// Tile this actor is standing on.
	//CField *m_tile;

	CClass *m_class;
};

