/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Tile code

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

#include "Graphics.h"
#include "field.h"

void CField::draw(float x, float y, float r, float g, float b)
{
	g_screen.drawQuad(x, y, 0.0, CCcolor(r, g, b));
}


void CField::setType(int type)
{
	m_type = type;
}

int CField::type()
{
	return m_type;
}

// Returns true if field is occupied.
bool CField::impassable()
{
	return m_impassable;
}

CField::CField()
{
	m_impassable = false;
}