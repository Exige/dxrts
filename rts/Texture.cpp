/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Texture & Texture atlas

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/


#include "stdio.h"
#include "SOIL2.h"
#include <windows.h>
#include <tchar.h> 
#include <stdio.h>
#include <strsafe.h>
#pragma comment(lib, "User32.lib")
#include "Texture.h"


CTexture::CTexture(const char *img)
{		
	// Load the image.
	m_img = SOIL_load_image(img, &m_width, &m_height, NULL, SOIL_LOAD_RGBA);

	m_startu = 0.0;
	m_endu = 0.0;
	m_startv = 0.0;
	m_endv = 0.0;
	m_id = 0;

	// Set name.
	m_name = img;

#ifdef WIN32
	int i = m_name.find_last_of('\\');
#else
	int i = m_name.find_last_of('/');
#endif
	m_name = m_name.substr(i+1, m_name.length() - i);

	// Also strip extension.
	i = m_name.find_first_of('.');

	m_name = m_name.substr(0, i);

	//m_name = "lal";
}

GLuint CTexture::loadBMP(const char *img)
{
	GLuint tex_2d = SOIL_load_OGL_texture
		(
		img,
		SOIL_LOAD_AUTO,
		SOIL_CREATE_NEW_ID,
		SOIL_FLAG_MIPMAPS | SOIL_FLAG_INVERT_Y | SOIL_FLAG_NTSC_SAFE_RGB | SOIL_FLAG_COMPRESS_TO_DXT
		);

	m_textureID = tex_2d;

	return tex_2d;
}

unsigned char *CTexture::img()
{
	return m_img;
}

std::string CTexture::name()
{
	return m_name;
}

GLuint CTexture::textureID()
{
	return m_textureID;
}

void CTexture::setID(int id)
{
	m_id = id;
}

int CTexture::width()
{
	return m_width;
}

int CTexture::height()
{
	return m_height;
}


void CTexture::setUVCoords(float ustart, float uend, float vstart, float vend)
{
	m_startu = ustart;
	m_endu = uend;

	m_startv = vstart;
	m_endv = vend;
}

CTextureAtlas::CTextureAtlas()
{
	m_currentid = 0;

	const char *path = "textures";

	WIN32_FIND_DATA ffd;
	LARGE_INTEGER filesize;
	TCHAR szDir[MAX_PATH];
	size_t length_of_arg;
	HANDLE hFind = INVALID_HANDLE_VALUE;
	DWORD dwError = 0;

	// If the directory is not specified as a command-line argument,
	// print usage.

	// Check that the input path plus 3 is not longer than MAX_PATH.
	// Three characters are for the "\*" plus NULL appended below.
	length_of_arg = 0;
	StringCchLength(path, MAX_PATH, &length_of_arg);

	if (length_of_arg > (MAX_PATH - 3))
	{
		//_tprintf(TEXT("\nDirectory path is too long.\n"));
		//return (-1);
	}

	//_tprintf(TEXT("\nTarget directory is %s\n\n"), argv[1]);

	// Prepare string for use with FindFile functions.  First, copy the
	// string to a buffer, then append '\*' to the directory name.

	StringCchCopy(szDir, MAX_PATH, path);
		StringCchCat(szDir, MAX_PATH, TEXT("\\*"));

	// Find the first file in the directory.

	hFind = FindFirstFile(szDir, &ffd);

	if (INVALID_HANDLE_VALUE == hFind)
	{
		//DisplayErrorBox(TEXT("FindFirstFile"));
		//return dwError;
	}

	// List all the files in the directory with some info about them.

	do
	{
		//if (ffd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
		//{
		//	_tprintf(TEXT("  %s   <DIR>\n"), ffd.cFileName);
		//}

		filesize.LowPart = ffd.nFileSizeLow;
		filesize.HighPart = ffd.nFileSizeHigh;
		
		std::string f = "textures\\";
			
		f += ffd.cFileName;
		unsigned int p = f.find_last_of('.png');
		if (p != f.npos)
		{			
			addTexture(CTexture(f.data()));
		}
	} while (FindNextFile(hFind, &ffd) != 0);	

	FindClose(hFind);	
}

CTextureAtlas::~CTextureAtlas()
{
	
}


void CTextureAtlas::addTexture(CTexture tex)
{
	m_textures.push_back(tex);
	tex.setID(m_currentid);
	m_currentid++;

	m_width += tex.width();
	if (m_height < tex.height())
		m_height = tex.height();	
}


void CTextureAtlas::build()
{
	glGetError();
	float u, v;
	u = v = 0;

	glGenTextures(1, &m_textureID);

	// "Bind" the newly created texture : all future texture functions will modify this texture
	glBindTexture(GL_TEXTURE_2D, m_textureID);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, m_width, m_height, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
	
	// Loop through all images. And set the corresponding u and v coords.
	for (int i = 0; i < m_textures.size(); i++)
	{
		float uend = (float)u / (float)m_width + (float)m_textures[i].width() / (float)m_width;
		//float vend = (float)v / (float)m_height + (float)m_textures[i].height() / (float)m_height;

		// Adjust to work witn an inverted y.		
		v = 1.0 - (float)m_textures[i].height() / float(m_height);
		float vend = 1.0;

		m_textures[i].setUVCoords((float)u/(float)m_width, uend, v, vend);

		// Now put this image in the big image data array.
		// to do: maybe we can use soil_load_ogl texture and then use subimage so we can make use of compressed textures
		// TO DO: free soil image data after usage here.
		glTexSubImage2D(GL_TEXTURE_2D, 0, u, v, m_textures[i].width(), m_textures[i].height(), GL_RGBA, GL_UNSIGNED_BYTE, m_textures[i].img());

		u = u + m_textures[i].width();	
	}
	
	unsigned char *img = new unsigned char[m_width * m_height * 4];
	glGetTexImage(GL_TEXTURE_2D, 0, GL_RGBA, GL_UNSIGNED_BYTE, img);


	// Free the texture again
	// Unbind texture.
	//glBindTexture(GL_TEXTURE_2D, 0);
	int a = GL_TEXTURE_2D;

	SOIL_save_image("lal.png", SOIL_SAVE_TYPE_PNG, m_width, m_height, 4, img);
	// Delete.
	glDeleteTextures(1, &m_textureID);

	// Now use SOIL to generate another opengl image.
	// Als je invert y doet klopt er niks meer van de uv's
	m_textureID = SOIL_load_OGL_texture("lal.png", 4, 0, SOIL_FLAG_MIPMAPS | SOIL_FLAG_COMPRESS_TO_DXT | SOIL_FLAG_INVERT_Y);
	
	glBindTexture(GL_TEXTURE_2D, m_textureID);

	// Set some parameters
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	// Enable blending
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	int err = glGetError();

}

CTexture *CTextureAtlas::getTextureFromId(int id)
{
	return &m_textures[id];
}



CTexture *CTextureAtlas::getTextureFromName(const  char *name)
{
	for (int i = 0; i < m_textures.size(); i++)
	{
		
		std::string s = m_textures[i].name();
		
		if (strcmp(s.c_str(), name) == 0)
		{
			return &m_textures[i];
		}
	}

	return NULL;
}