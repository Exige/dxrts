/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Program entrypoint

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <ctime>

// Include GLEW. Always include it before gl.h and glfw.h, since it's a bit magic.
#include "GL/glew.h"


// Include GLFW
#include <GLFW/glfw3.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"
#include <glm/gtc/type_ptr.hpp>

#include "shading.h"
#include "Graphics.h"
#include "map.h"
#include "Input.h"
#include "Texture.h"
#include "Actor.h"
#include "ActorCollection.h"

void
ETB_GL_ERROR_CALLBACK(GLenum        source,
GLenum        type,
GLuint        id,
GLenum        severity,
GLsizei       length,
const GLchar* message,
GLvoid*       userParam)
{
	exit(1);
}

GLdouble g_x, g_y, g_z;

CActor *a1;
CActorCollection *c1;

void window_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
	g_screen.setAspect(width / height);
}

void mouse_button_callback(GLFWwindow* window, int button, int action, int mods)
{
	if (button == GLFW_MOUSE_BUTTON_LEFT && action == GLFW_PRESS)
	{
		GLint v[4];
		v[0] = 0;
		v[1] = 0;
		v[2] = 1024;
		v[3] = 768;
		
		GLdouble xpos, ypos;
		GLfloat zpos;		
		glfwGetCursorPos(window, &xpos, &ypos);
		ypos = (GLdouble)v[3] - ypos;
		glReadPixels(xpos, ypos, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &zpos);
		
		glm::vec3 win(xpos, ypos, zpos);		
		glm::vec4 vw(0.0, 0.0, 1024.0, 768.0);
		glm::vec3 uu = glm::unProject(win, g_screen.view() * g_screen.model(), g_screen.projection(), vw);
		g_x = uu.x;
		g_y = uu.y;
		g_z = uu.z;
		
		for (int i = 0; i < c1->size(); i++)
		{
			CActor *a = c1->getActorFromId(i);
			if (g_x > a->x() - 2.0 && g_x < a->x() + 2.0
				&& g_y > a->y() - 2.0 && g_y < a->y() + 2.0)
			{
				a->select();
			}
			else
			{
				a->deselect();
			}
		}		
	}

	else if (button == GLFW_MOUSE_BUTTON_RIGHT && action == GLFW_PRESS)
	{
		for (int i = 0; i < c1->size(); i++)
		{
			CActor *a = c1->getActorFromId(i);
			if (a->isSelected())
			{
				GLint v[4];
				v[0] = 0;
				v[1] = 0;
				// Change this when resolution changes
				v[2] = 1024;
				v[3] = 768;

				GLdouble xpos, ypos;
				GLfloat zpos;
				glfwGetCursorPos(window, &xpos, &ypos);
				ypos = (GLdouble)v[3] - ypos;
				glReadPixels(xpos, ypos, 1, 1, GL_DEPTH_COMPONENT, GL_FLOAT, &zpos);

				glm::vec3 win(xpos, ypos, zpos);
				glm::vec4 vw(0.0, 0.0, 1024.0, 768.0);
				glm::vec3 uu = glm::unProject(win, g_screen.view() * g_screen.model(), g_screen.projection(), vw);

				a->setDestination(uu.x, uu.y);				
			}
		}
	}
}


void main()
{	
	// Initialise GLFW
	
	glm::vec2 u(sqrt(2), 0);
	glm::vec2 v(0, -sqrt(2));
	glm::vec2 s(1, 1);

	glm::vec2 proju = (glm::dot(u, s) / glm::dot(s, s))*s;
	glm::vec2 projv = (glm::dot(v, s) / glm::dot(s, s))*s;
	
	float dx = abs(proju.x - projv.x);
	float dy = abs(proju.y - projv.y);

	float dis = sqrt(dx*dx + dy*dy);

	if (!glfwInit())
	{
		fprintf(stderr, "Failed to initialize GLFW\n");
	}

	g_x = 0.0;
	g_y = 0.0;
	g_z = 0.0;
	
	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
	
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE); // To make MacOS happy; should not be needed
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL 

	// Open a window and create its OpenGL context 	
	g_window = glfwCreateWindow(1024, 768, "Tutorial 01", NULL, NULL);
	glViewport(0, 0, 1024, 768);
	//g_screen.setAspect(1024.0 / 768.0);

	if (g_window == NULL)
	{
		fprintf(stderr, "Failed to open GLFW window. If you have an Intel GPU, they are not 3.3 compatible. Try the 2.1 version of the tutorials.\n");
		glfwTerminate();
		return;
	}
	 
	// Set resize callback.
	glfwSetWindowSizeCallback(g_window, window_size_callback);

	glfwMakeContextCurrent(g_window); // Initialize GLEW 
	glewExperimental = true; // Needed in core profile 
	if (glewInit() != GLEW_OK)
	{
		fprintf(stderr, "Failed to initialize GLEW\n");
	}

	glGetError();



	GLuint VertexArrayID;
	glGenVertexArrays(1, &VertexArrayID);
	glBindVertexArray(VertexArrayID);
	
	
	// Ensure we can capture the escape key being pressed below
	glfwSetInputMode(g_window, GLFW_STICKY_KEYS, GL_TRUE);
	//glfwSetInputMode(g_window, GLFW_CURSOR, GLFW_CURSOR_NORMAL);

	glfwSetMouseButtonCallback(g_window, mouse_button_callback);


	// Enable depth test
	glEnable(GL_DEPTH_TEST);
	// Accept fragment if it closer to the camera than the former one
	glDepthFunc(GL_LESS);

	glClearColor(0.0f, 0.0f, 0.4f, 0.0f);
	CTextureAtlas atlas;
	g_controls = new CInput;
	g_screen = CGraphics(atlas);
	
	g_screen.setAspect(1024.0 / 768.0);

	//CTexture tex("tex1.png");
	//CTexture tex2("tex2.png");
	//atlas.addTexture(tex);
	//atlas.addTexture(tex2);

	atlas.build();
	g_map = new CMap;
	//CActor a1(atlas.getTextureFromId(1));
	//a1 = new CActor(atlas.getTextureFromId(1));

	//tex.loadBMP("vile.bmp");
	clock_t t = clock();
		
	// We run the main loop every 1/35 of a second.
	int frametime = CLOCKS_PER_SEC / 35;
	double lastTime = glfwGetTime();
	int nbFrames = 0;



	
	c1 = new CActorCollection;
	c1->spawn(2, 2);

	do{

		if (clock() < t + frametime)
		{		
			continue;
		}
		
		t = clock();
		g_controls->getInput();
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	

		// Measure speed
		double currentTime = glfwGetTime();
		nbFrames++;
		if (currentTime - lastTime >= 1.0){ // If last prinf() was more than 1 sec ago
			// printf and reset timer
			printf("%f ms/frame\n",  double(nbFrames));
			nbFrames = 0;
			lastTime += 1.0;
		}
		
		
	//	g_screen.drawQuad(0.0, 0.0, 0.0, CCcolor(1.0, 0.0, 1.0));
	//	g_screen.drawSprite(7.0, 0.0, 0.0, *(atlas.getTextureFromId(0)));
		//g_screen.drawSprite(0.0, 0.0, 0.0, *atlas.getTextureFromId(1));
		//g_screen.drawSprite(g_x, g_y, 0.001, *atlas.getTextureFromId(2));
		/*
		g_screen.drawQuad(3.0, 0.0, 0.0, CCcolor(1.0, 1.0, 1.0));
		
		g_screen.drawQuad(0.0, -2.0, 0.0, CCcolor(0.5, 0.5, 0.5));
		g_screen.drawQuad(-3.0, 0.0, 0.0, CCcolor(1.0, 0.0, 0.0));

		g_screen.drawQuad(0.0, 5.0, 0.0, CCcolor(0.4, 0.0, 0.0));
		g_screen.drawQuad(2.0, 5.0, 0.0, CCcolor(1.0, 0.0, 0.0));
		g_screen.drawQuad(4.0, 5.0, 0.0, CCcolor(1.0, 0.0, 0.0));
		g_screen.drawQuad(6.0, 5.0, 0.0, CCcolor(1.0, 0.0, 0.0));
		g_screen.drawQuad(8.0, 5.0, 0.0, CCcolor(1.0, 0.0, 0.0));
		g_screen.drawQuad(10.0, 5.0, 0.0, CCcolor(0.0, 1.0, 0.0));
	//	g_screen.drawQuad(12.0, 5.0, 0.0, CCcolor(1.0, 0.0, 1.0));
	*/
		
		//printf("just before drawing\n");
		g_map->draw();
		c1->draw();
		//a1->tick();
		//g_screen.render();		
		// Swap buffers
		glfwSwapBuffers(g_window);
		glfwPollEvents();

	} // Check if the ESC key was pressed or the window was closed
	while (glfwGetKey(g_window, GLFW_MOUSE_BUTTON_1) != GLFW_PRESS &&
	glfwWindowShouldClose(g_window) == 0);
}



























