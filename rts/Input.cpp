/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Keyboard & mouse input

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/


#include "Input.h"
#include "Graphics.h"
#include "glm/gtx/rotate_vector.hpp"

CInput *g_controls;

CInput::CInput()
{

}


CInput::~CInput()
{
}

void CInput::getInput()
{
	// Move forward
	if (glfwGetKey(g_window, GLFW_KEY_UP) == GLFW_PRESS)
	{
		glm::vec3 v = glm::vec3(0.0, -1.0, 0.0);
		//v = glm::rotate(v, -glm::pi<float>()/4.0f, glm::vec3(0.0, 0.0, 1.0));
		g_screen.Changeview(v);
	}
	// Move backward
	if (glfwGetKey(g_window, GLFW_KEY_DOWN) == GLFW_PRESS)
	{
		glm::vec3 v = glm::vec3(0.0, 1.0, 0.0);
	//	v = glm::rotate(v, -glm::pi<float>() / 4.0f, glm::vec3(0.0, 0.0, 1.0));
		g_screen.Changeview(v);
	}
	// Strafe right
	if (glfwGetKey(g_window, GLFW_KEY_RIGHT) == GLFW_PRESS)
	{
		glm::vec3 v = glm::vec3(-1.0, 0.0, 0.0);
	//	v = glm::rotate(v, -glm::pi<float>() / 4.0f, glm::vec3(0.0, 0.0, 1.0));
		g_screen.Changeview(v);
	}
	// Strafe left
	if (glfwGetKey(g_window, GLFW_KEY_LEFT) == GLFW_PRESS)
	{
		glm::vec3 v = glm::vec3(1.0, 0.0, 0.0);
	//	v = glm::rotate(v, -glm::pi<float>() / 4.0f, glm::vec3(0.0, 0.0, 1.0));
		g_screen.Changeview(v);
	}
}


