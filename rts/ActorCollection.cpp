/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Collections of actors

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

#include "glm/gtc/matrix_transform.hpp"
#include "ActorCollection.h"
#include "map.h"

#define MAX_BUFFER_SIZE 14400

CActorCollection::CActorCollection()
	:CDrawAble()
{	
	CTexture *tex = g_screen.textures()->getTextureFromId(2);
	CActor act(tex);	
	m_actors.push_back(act);

	GLfloat uv[12];
	std::vector<GLfloat> vertices;
	std::vector<GLfloat> uvs;
	
	for (int j = 0; j < m_actors.size();j++)
	{
		glm::vec3 v(m_actors[j].x(), m_actors[j].y(), 0.001);
		glm::mat4 m(1.0f);
		m = glm::translate(m, v);

		CTexture *tex = m_actors[j].texture();
		
		uv[0] = tex->startu();
		uv[1] = tex->startv();
		uv[2] = tex->endu();
		uv[3] = tex->startv();
		uv[4] = tex->startu();
		uv[5] = tex->endv();
		uv[6] = tex->startu();
		uv[7] = tex->endv();
		uv[8] = tex->endu();
		uv[9] = tex->startv();
		uv[10] = tex->endu();
		uv[11] = tex->endv();

		for (int i = 0; i < 6; i++)
		{
			
			glm::vec4 u(m_quad[i * 3], m_quad[i * 3 + 1], m_quad[i * 3 + 2], 1);

			u = m*u;

			vertices.push_back(u.x);
			vertices.push_back(u.y);
			vertices.push_back(u.z);

			uvs.push_back(uv[i * 2]);
			uvs.push_back(uv[i * 2 + 1]);

			
		}

	}
	
	int g = vertices.size()* sizeof(vertices[0]) * 1000;
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, g, NULL, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size()*sizeof(GLfloat), vertices.data());

	GLint err = glGetError();
	glBindBuffer(GL_ARRAY_BUFFER, m_attributebuffer);
	glBufferData(GL_ARRAY_BUFFER, MAX_BUFFER_SIZE*sizeof(GLfloat), NULL, GL_STREAM_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, uvs.size()*sizeof(GLfloat), uvs.data());
	
}


CActorCollection::~CActorCollection()
{
}

void CActorCollection::draw()
{
	m_collisiondetector.detect(this);

	GLfloat *buf = NULL;
	GLint err = glGetError();

	glGetError();
	glm::mat4 model = glm::mat4(1.0);	

	// Setup transformation matrix.
	glm::mat4 MVP = g_screen.projection() * g_screen.view() * model;
	
	// Bind the vertex buffer.
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffer);

	// Get the buffer.
	buf = (GLfloat*)glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE);

	// Keep a seperate list of selected actors so we can draw highlight squares later.
	std::vector<CActor *> selectedactors;
	
	// Update positions
	for (int i = 0; i < m_actors.size(); i++)
	{
		// Move this out of here to a seperate function.
		m_actors[i].move();		

		// If actor is selected move it to the selected actors list
		// so we can highlight it later.
		if (m_actors[i].isSelected())
		{
			selectedactors.push_back(&m_actors[i]);
		}

		// Update screen position.
		glm::vec3 v(m_actors[i].x(), m_actors[i].y(), 0.001);
		glm::mat4 m(1.0f);
		m = glm::translate(m, v);

			
		std::vector<GLfloat> vertices;		

		for (int j = 0; j < 6; j++)
		{
			glm::vec4 u(m_quad[j * 3], m_quad[j * 3 + 1], m_quad[j * 3 + 2], 1);

			u = m*u;
				
			vertices.push_back(u.x);
			vertices.push_back(u.y);
			vertices.push_back(u.z);
		}
		buf = buf + i * 18;
		memcpy(buf, vertices.data(), sizeof(GLfloat) * 18);			
		
	}

	// go to next empty index in vertex buffer.
	buf += 18;
	std::vector<GLfloat> vertices2;

	// Now loop over the selected actors to add a selection marker.
	for (int i = 0; i < selectedactors.size(); i++)
	{
		glm::vec3 v(selectedactors[i]->x(), selectedactors[i]->y(), 0.01);
		glm::mat4 m(1.0f);
		m = glm::translate(m, v);
		m = glm::rotate(m, glm::pi<float>() / 4.0f, glm::vec3(0.0, 0.0, 1.0));

		std::vector<GLfloat> vertices;

		for (int j = 0; j < 6; j++)
		{
			glm::vec4 u(m_quad[j * 3], m_quad[j * 3 + 1], m_quad[j * 3 + 2], 1);			
			u = m*u;

			vertices2.push_back(u.x);
			vertices2.push_back(u.y);
			vertices2.push_back(u.z);
		}
		buf = buf + i * 18;
		memcpy(buf, vertices2.data(), 18 * sizeof(GLfloat));
	}

	glUnmapBuffer(GL_ARRAY_BUFFER);	

	// Bind attribute buffer to read textures.
	glBindBuffer(GL_ARRAY_BUFFER, m_attributebuffer);

	// Get the buffer.
	buf = (GLfloat*)glMapBuffer(GL_ARRAY_BUFFER, GL_READ_WRITE);

	err = glGetError();

	// Update textures.
	for (int i = 0; i < m_actors.size(); i++)
	{
		GLfloat uv[12];
		std::vector<GLfloat> uvs;
	
		// Get current actor texture.
		CTexture *tex = m_actors[i].texture();

		// Set UV coordinates properly.
		uv[0] = tex->startu();
		uv[1] = tex->startv();
		uv[2] = tex->endu();
		uv[3] = tex->startv();
		uv[4] = tex->startu();
		uv[5] = tex->endv();
		uv[6] = tex->startu();
		uv[7] = tex->endv();
		uv[8] = tex->endu();
		uv[9] = tex->startv();
		uv[10] = tex->endu();
		uv[11] = tex->endv();

		for (int i = 0; i < 6; i++)
		{
			uvs.push_back(uv[i * 2]);
			uvs.push_back(uv[i * 2 + 1]);
		}

		// Copy the modified vertices into the buffer.
		assert(buf != NULL);
		buf = buf + i * 12;
		memcpy(buf, uvs.data(), sizeof(GLfloat) * 12);
	}

	// Update the buffer.
	//buf += 12;
	/*
	// Also loop over the selected actors.
	for (int i = 0; i < selectedactors.size(); i++)
	{
		///glm::vec3 v(selectedactors[i]->x() + 3, selectedactors[i]->y() + 3, 0.1);
		//glm::mat4 m(1.0f);
		//m = glm::translate(m, v);

		std::vector<GLfloat> colors;

		for (int j = 0; j < 6; j++)
		{
			//glm::vec4 u(m_quad[j * 3], m_quad[j * 3 + 1], m_quad[j * 3 + 2], 1);

			//u = m*u;
			colors.push_back(1.0);
			colors.push_back(1.0);
			colors.push_back(1.0);
		}
		buf = buf + i * 18;
		memcpy(buf, colors.data(), sizeof(GLfloat) * 18);
	}
	*/

	glUnmapBuffer(GL_ARRAY_BUFFER);


	glUseProgram(g_screen.texProgramID());

	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID = glGetUniformLocation(g_screen.texProgramID(), "myTextureSampler");
	GLuint MatrixID = glGetUniformLocation(g_screen.texProgramID(), "MVP");
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	err = glGetError();
	glBindTexture(GL_TEXTURE_2D, g_screen.textures()->textureID());



	// Set our "myTextureSampler" sampler to user Texture Unit 0
	glUniform1i(TextureID, 0);


	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffer);	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	GLint sz;
	sz = (m_actors.size()) * 6;
	//glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &sz);
	//sz = sz / sizeof(GLfloat);
	

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);	
	glBindBuffer(GL_ARRAY_BUFFER, m_attributebuffer);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);	

	glDrawArrays(GL_TRIANGLES, 0, sz);


	glBindTexture(GL_TEXTURE_2D, 0);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);


	/////////////////////////////////////////////

	// Enable blending to make the selection markers translucent.
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffer);

	// Use shader for drawing in primitive colors.
	glUseProgram(g_screen.programID());

	MatrixID = glGetUniformLocation(g_screen.programID(), "MVP");
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffer);
	glVertexAttribPointer(
		0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
		3,                  // size
		GL_FLOAT,           // type
		GL_FALSE,           // normalized?
		0,                  // stride
		(void*)0            // array buffer offset
		);
	sz = (selectedactors.size()) * 6;
	glDrawArrays(GL_TRIANGLES, m_actors.size()*6, sz); // 3 indices starting at 0 -> 1 triangle

	glDisableVertexAttribArray(0);

	

	//////////////////////////////////////////////

	err = glGetError();
}

// Spawns a new actor on tile [x, y].
// Returns false if not successful.
bool CActorCollection::spawn(int x, int y)
{
	// Only spawn here if field is empty
	if (g_map->occupied(x, y))
		return false;

	// Create new actor.
	CTexture *tex = g_screen.textures()->getTextureFromId(2);
	CActor act(tex);

	// spawn in middle of tile.
	float mx, my;
	// width = height for now. Should be in actor class properties.
	float width = 1;

	float xpadding = (1 - width) / 2;
	float ypadding = (1 - width) / 2;

	act.setPosition((float)x + xpadding, (float)y + ypadding);
	act.setDestination((float)x + xpadding, (float)y + ypadding);
	
	m_actors.push_back(act);
	return true;
}
