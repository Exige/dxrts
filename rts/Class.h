/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Class of an actor.

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

#pragma once

#include <string>
class CClass
{
public:
	CClass(const char *name, float width);	

	int width();

private:
	std::string m_name;

	// Width of the bounding box.
	float m_width;
	
};



class CUnit : public CClass
{
public:
	CUnit(const char *name, float width);

	int speed();
	void setBattleStats(int hp, int armor, int piercearmor, int attack, int speed);

private:
	int m_basehp;
	int m_basearmor;
	int m_basepiercearmor;
	int m_baseattack;
	int m_basespeed;
};


extern CUnit *testunit;



