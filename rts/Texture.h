/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Texture & Texture atlas

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

#ifndef _TEXTURE_H_
#define _TEXTURE_H_

#include <vector>
// Include GLEW. Always include it before gl.h and glfw.h, since it's a bit magic.
#include "GL/glew.h"

// Include GLFW
#include <GLFW/glfw3.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

class CTexture
{
public:
	CTexture(const char *img);
	GLuint textureID();
	GLuint loadBMP(const char *img);
	GLuint loadTGA_glfw(const char * img);

	unsigned char *img();

	void setID(int id);
	int width();
	int height();

	float startu() { return m_startu; }
	float startv() { return m_startv; }

	float endu() { return m_endu;  }
	float endv() { return m_endv; }

	void setUVCoords(float ustart, float uend, float vstart, float vend);

	std::string name();
private:	
	GLuint m_textureID;	

	unsigned char *m_img;
	int m_width;
	int m_height;

	float m_startu;
	float m_endu;

	float m_startv;
	float m_endv;

	std::string m_name;

	int m_id;
};


class CTextureAtlas
{
public:
	CTextureAtlas();
	~CTextureAtlas();
	void addTexture(CTexture tex);
	void build();

	GLuint textureID() { return m_textureID; }

	CTexture *getTextureFromId(int id);
	CTexture *getTextureFromName(const char *name);
private:
	GLuint m_uvbuffer;
	GLuint m_textureID;

	std::vector <CTexture> m_textures;

	int m_width;
	int m_height;

	int m_currentid;

};

#endif _TEXTURE_H_