/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: General drawing code

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

#pragma once
// Include standard headers
#include <stdio.h>
#include <stdlib.h>
#include <vector>

// Include GLEW. Always include it before gl.h and glfw.h, since it's a bit magic.
#include "GL/glew.h"

// Include GLFW
#include <GLFW/glfw3.h>

#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "shading.h"
#include "Texture.h"


class CCcolor
{
public:
	CCcolor(float _r, float _g, float _b)
	{
		r = _r;
		g = _g;
		b = _b;
	}
	float r, g, b;
};


class CGraphics
{
public:
	CGraphics(CTextureAtlas &atlas);
	CGraphics() { }
	~CGraphics();

	void drawQuad(float x, float y, float z, CCcolor clr);
	void drawQuad(float x, float y, float z, CTexture tex);
	void drawSprite(float x, float y, float z, CTexture tex);

	void Changeview(glm::vec3 v);

	void setAspect(float aspect);
	float aspect();
	GLuint *vertexBuffer() { return &m_vertexbuffer; }
	GLuint *colorBuffer() { return &m_colorbuffer; }
	void render();	

	glm::mat4 model();
	glm::mat4 view();
	glm::mat4 projection();

	void setNPolygons(int n) { m_npolygons = n; }

	GLuint programID() { return m_programID; }
	GLuint texProgramID() { return m_texprogramID; }

	CTextureAtlas *textures() { return m_textures; }




private:
	void fillBuffer(int type);
	
	GLuint m_vertexbuffer;	
	GLuint m_colorbuffer;
	unsigned int m_npolygons;
	GLuint m_programID;
	GLuint m_texprogramID;
	GLuint m_uvbufferID;

	glm::mat4 m_model;
	glm::mat4 m_view;
	glm::mat4 m_projection;

	std::vector<GLfloat> m_vertices;
	std::vector<GLfloat> m_texvertices;

	std::vector<GLfloat> m_vertexcolors;
	std::vector<GLfloat> m_uvbuffer;

	CTextureAtlas *m_textures;

	float m_aspect;
};

class CDrawAble
{
public:
	CDrawAble();
	virtual void draw() { }
	GLuint vertexBuffer() { return m_vertexbuffer; }
	GLuint attributebuffer() { return m_attributebuffer; }
protected:
	GLuint m_vertexbuffer;
	GLuint m_attributebuffer;	
};



extern CGraphics g_screen;
extern GLFWwindow* g_window; // (In the accompanying source code, this variable is global) 

static GLfloat m_quad[18] =
{
	// triangle
	-1.0f, -1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	-1.0f, 1.0f, 0.0f,

	-1.0f, 1.0f, 0.0f,
	1.0f, -1.0f, 0.0f,
	1.0f, 1.0f, 0.0f,
};