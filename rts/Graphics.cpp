/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: General drawing code

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

#include "Graphics.h"
#include "glm/gtc/matrix_transform.hpp"
#include "SOIL2.h"
#include "image_helper.h"


CGraphics g_screen;
GLFWwindow* g_window;


CDrawAble::CDrawAble()
{
	glGenBuffers(1, &m_vertexbuffer);
	glGenBuffers(1, &m_attributebuffer);
}


CGraphics::CGraphics(CTextureAtlas &atlas) 
{
	m_textures = &atlas;
	m_programID = LoadShaders("SimpleVertexShader.vertexshader", "SimpleFragmentShader.fragmentshader");
	m_texprogramID = LoadShaders("TextureVertexShader.vertexshader", "TextureFragmentShader.fragmentshader");

	m_aspect = 1024.0 / 768.0; 

	m_npolygons = 0;

	glGenBuffers(1, &m_vertexbuffer);
	glGenBuffers(1, &m_colorbuffer);
	glGenBuffers(1, &m_uvbufferID);	

	// Set up view and projection.
	m_model = glm::mat4(1.0f);  // Changes for each model !

	// Projection matrix : 45� Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
	m_projection = glm::perspective(45.0f, m_aspect, 0.1f, 100.0f);
	// Camera ma	

	m_view = glm::lookAt(glm::vec3(0.0, -5.0,20.0), glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	//m_view = glm::rotate(m_view, glm::pi<float>() / 4.0f, glm::vec3(0.0, 0.0, 1.0));	

}

glm::mat4 CGraphics::model() 
{
	return m_model;
}

glm::mat4 CGraphics::view()
{
	return m_view;
}

glm::mat4 CGraphics::projection()
{
	return m_projection;
}



void CGraphics::drawQuad(float x, float y, float z, CCcolor clr)
{
	// Make the translation vector.
	glm::vec3 v(x, y, z);
	glm::mat4 m(1.0f);
	m = glm::translate(m, v);

	for (int i = 0; i < 6; i++)
	{
		glm::vec4 u(m_quad[i * 3], m_quad[i * 3 + 1], m_quad[i * 3 + 2], 1);

		u = m*u;	
	
		m_vertices.push_back(u.x);
		m_vertices.push_back(u.y);
		m_vertices.push_back(u.z);

		m_vertexcolors.push_back(clr.r);
		m_vertexcolors.push_back(clr.g);
		m_vertexcolors.push_back(clr.b);
	}
}

void CGraphics::drawSprite(float x, float y, float z, CTexture tex)
{
	GLfloat uv[] =
	{
		0.0, 0.0,
		1.0, 0.0,
		0.0, 1.0,

		0.0, 1.0,
		1.0, 0.0,
		1.0, 1.0,
	};

	glm::vec3 v(x, y, z);
	glm::mat4 m(1.0f);
	m = glm::translate(m, v);
	
	uv[0] = tex.startu();
	uv[1] = tex.startv();
	uv[2] = tex.endu();
	uv[3] = tex.startv();
	uv[4] = tex.startu();
	uv[5] = tex.endv();
	uv[6] = tex.startu();
	uv[7] = tex.endv();
	uv[8] = tex.endu();
	uv[9] = tex.startv();
	uv[10] = tex.endu();
	uv[11] = tex.endv();

	for (int i = 0; i < 6; i++)
	{
		glm::vec4 u(m_quad[i * 3], m_quad[i * 3 + 1], m_quad[i * 3 + 2], 1);

		u = m*u;

		m_texvertices.push_back(u.x);
		m_texvertices.push_back(u.y);
		m_texvertices.push_back(u.z);

		m_uvbuffer.push_back(uv[i*2]);
		m_uvbuffer.push_back(uv[i * 2 + 1]);
	}
}

void CGraphics::render()
{
	m_model = glm::mat4(1.0);
	glm::mat4 MVP = m_projection * m_view * m_model;
	GLuint MatrixID;
	MatrixID = glGetUniformLocation(m_programID, "MVP");
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

	
	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);	

	glUseProgram(m_programID);

	glBindBuffer(GL_ARRAY_BUFFER, m_colorbuffer);
	//glBufferData(GL_ARRAY_BUFFER, m_vertexcolors.size()*sizeof(GLfloat), m_vertexcolors.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffer);	
	//glBufferData(GL_ARRAY_BUFFER, m_vertices.size()*sizeof(GLfloat), m_vertices.data(), GL_STATIC_DRAW);
	
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	glDrawArrays(GL_TRIANGLES, 0, m_npolygons);
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);	
	/*
	// Get a handle for our "myTextureSampler" uniform
	GLuint TextureID = glGetUniformLocation(m_texprogramID, "myTextureSampler");
	MatrixID = glGetUniformLocation(m_texprogramID, "MVP");
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);

	glUseProgram(m_texprogramID);

	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, m_textures->textureID());
	// Set our "myTextureSampler" sampler to user Texture Unit 0
	glUniform1i(TextureID, 0);	

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffer);
	
	glBufferData(GL_ARRAY_BUFFER, m_texvertices.size()*sizeof(GLfloat), m_texvertices.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);	

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, m_uvbufferID);
	glBufferData(GL_ARRAY_BUFFER, m_uvbuffer.size()*sizeof(GLfloat), m_uvbuffer.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glDrawArrays(GL_TRIANGLES, 0, m_npolygons / 3);	

	glBindTexture(GL_TEXTURE_2D, 0);	
	
	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);

	//m_vertices.clear();
	//m_vertexcolors.clear();
	m_texvertices.clear();
	m_uvbuffer.clear();

	*/
}

void CGraphics::Changeview(glm::vec3 v)
{
	glm::mat4 m(1.0f);
	m = glm::translate(m, v);
	m_view = m_view * m;
}


void CGraphics::setAspect(float aspect)
{
	m_aspect = aspect;
	m_projection = glm::perspective(45.0f, m_aspect, 0.1f, 100.0f);
	m_aspect = 1.0;
	
}

float CGraphics::aspect()
{
	return m_aspect;
}

CGraphics::~CGraphics()
{
	
}
