﻿/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: A map is a collection of tiles (CField)

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

#include "map.h"
#include "Graphics.h"
#include "windows.h"

CMap *g_map;

void CMap::create()
{

}

CMap::CMap()
	:CDrawAble()
{
	
	CTexture *tex = g_screen.textures()->getTextureFromId(2);
	std::vector<GLfloat> vertices;
	std::vector<GLfloat> uvs;
	GLfloat uv[12];

	m_offset = 0;


	// Initialize field.
	m_map = new CField*[32];	
	for (int i = 0; i < 32; i++)
	{
		m_map[i] = new CField[32];

		for (int j = 0; j < 32; j++)
		{
			m_map[i][j].setType(rand() % 2);

			if (m_map[i][j].type() == TYPE_STONE)
			{
				tex = g_screen.textures()->getTextureFromName("stone");
			}

			else if (m_map[i][j].type() == TYPE_GRASS)
			{
				tex = g_screen.textures()->getTextureFromName("GRASS2");
			}

			else
			{
				exit(0);
			}

			m_map[i][j].n = i * 32 + j;
			float x = i*2.0;
			float y = j * 2.0;
			float z = 0.0;
			
			// Make the translation vector.
			glm::vec3 v(x, y, z);
			glm::mat4 m(1.0f);
			m = glm::translate(m, v);

			uv[0] = tex->startu();
			uv[1] = tex->startv();
			uv[2] = tex->endu();
			uv[3] = tex->startv();
			uv[4] = tex->startu();
			uv[5] = tex->endv();
			uv[6] = tex->startu();
			uv[7] = tex->endv();
			uv[8] = tex->endu();
			uv[9] = tex->startv();
			uv[10] = tex->endu();
			uv[11] = tex->endv();

			for (int i = 0; i < 6; i++)
			{
				
				glm::vec4 u(m_quad[i * 3], m_quad[i * 3 + 1], m_quad[i * 3 + 2], 1);

				u = m*u;	

				vertices.push_back(u.x);
				vertices.push_back(u.y);
				vertices.push_back(u.z);

				uvs.push_back(uv[i * 2]);
				uvs.push_back(uv[i * 2 + 1]);
				
			}
		}
	}
	int s = vertices.size()*sizeof(GLfloat);

	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffer);
	glBufferData(GL_ARRAY_BUFFER, vertices.size()*sizeof(GLfloat), vertices.data(), GL_STATIC_DRAW);

	glBindBuffer(GL_ARRAY_BUFFER, m_attributebuffer);
	glBufferData(GL_ARRAY_BUFFER, uvs.size()*sizeof(GLfloat), uvs.data(), GL_STATIC_DRAW);

	
}

void CMap::draw()
{

	glGetError();
	glm::mat4 model = glm::mat4(1.0);
	//model = glm::rotate(model, glm::pi<float>() / 6.0f, glm::vec3(1.0, 0.0, 0.0));
	//model = glm::scale(model, glm::vec3(0.5, 0.5, 0.5));
	model = glm::rotate(model, glm::pi<float>() / 4.0f, glm::vec3(0.0, 0.0, 1.0));
	glm::mat4 MVP = g_screen.projection() * g_screen.view() * model;

	glUseProgram(g_screen.texProgramID());

	GLint t = g_screen.texProgramID();
	
	// Get a handle for our "myTextureSampler" uniform
	GLint TextureID = glGetUniformLocation(g_screen.texProgramID(), "myTextureSampler");
	GLint MatrixID = glGetUniformLocation(g_screen.texProgramID(), "MVP");
	glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);


	// Bind our texture in Texture Unit 0
	glActiveTexture(GL_TEXTURE0);
	GLint a = g_screen.textures()->textureID();
	glBindTexture(GL_TEXTURE_2D, g_screen.textures()->textureID());
	// Set our "myTextureSampler" sampler to user Texture Unit 0

	glUniform1i(TextureID, 0);	
	

	glEnableVertexAttribArray(0);
	glBindBuffer(GL_ARRAY_BUFFER, m_vertexbuffer);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 0, (void*)0);
	

	GLint sz;
	glGetBufferParameteriv(GL_ARRAY_BUFFER, GL_BUFFER_SIZE, &sz);
	sz = sz / sizeof(GLfloat);	

	// 2nd attribute buffer : UVs
	glEnableVertexAttribArray(1);
	glBindBuffer(GL_ARRAY_BUFFER, m_attributebuffer);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 0, (void*)0);

	glDrawArrays(GL_TRIANGLES, 0, sz / 3);
	glBindTexture(GL_TEXTURE_2D, 0);

	glDisableVertexAttribArray(0);
	glDisableVertexAttribArray(1);
	int err = glGetError();
	
	
}

// Returns true if field is occupied.
bool CMap::occupied(int x, int y)
{
	return m_map[x][y].impassable();
}

