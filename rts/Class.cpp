/*
This file is part of RTS.

Author: Thomas Nijman
Purpose: Class of an actor.

This work is licensed under the Creative Commons Attribution - NonCommercial - NoDerivatives 4.0 International License.
To view a copy of this license, visit http ://creativecommons.org/licenses/by-nc-nd/4.0/
or send a letter to Creative Commons, PO Box 1866, Mountain View, CA 94042, USA.
*/

#include "Class.h"

CUnit *testunit;

CClass::CClass(const char *name, float width)
{
	m_name = name;
	m_width = 2;
}

int CClass::width()
{
	return m_width;
}


CUnit::CUnit(const char *name, float width)
	:CClass(name, width)
{

}

int CUnit::speed()
{
	return m_basespeed;
}

void CUnit::setBattleStats(int hp, int armor, int piercearmor, int attack, int speed)
{
	m_basehp = hp;
	m_basearmor = armor;
	m_basepiercearmor = piercearmor;
	m_baseattack = attack;
	m_basespeed = speed;
}